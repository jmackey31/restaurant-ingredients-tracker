import React from 'react';
import ReactDOM from 'react-dom';
// @ts-ignore
import {BrowserRouter, Route, Switch} from 'react-router-dom';
// @ts-ignore
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import reduxThunk from 'redux-thunk';

// Reducers
import reducers from './app-redux/reducers';

import './index.css';
import App from './App';
import TopBarComponent from './Components/TopBarComponent/TopBarComponent';
import StatusBarComponent from './Components/StatusBarComponent.tsx/StatusBarComponent';
import App404Component from './Components/App404Component/App404Component';
import DashboardComponent from './Containers/DashboardComponent/DashboardComponent';
import CancelledIngredientsComponent from './Components/CancelledIngredientsComponent/CancelledIngredientsComponent';
import PendingIngredientsComponent from './Components/PendingIngredientsComponent/PendingIngredientsComponent';
import InProgressIngredientsComponent from './Components/InProgressIngredientsComponent/InProgressIngredientsComponent';
import FulfilledIngredientsComponent from './Components/FulfilledIngredientsComponent/FulfilledIngredientsComponent';

import * as serviceWorker from './serviceWorker';

const store = createStore(
    reducers,
    {},
    applyMiddleware(reduxThunk)
);

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <App>
                <TopBarComponent />
                <StatusBarComponent />
                <Switch>
                    <Route path="/" exact component={DashboardComponent}/>
                    <Route path="/cancelled" exact component={CancelledIngredientsComponent}/>
                    <Route path="/pending" exact component={PendingIngredientsComponent}/>
                    <Route path="/in-progress" exact component={InProgressIngredientsComponent}/>
                    <Route path="/fulfilled" exact component={FulfilledIngredientsComponent}/>
                    <Route exact component={App404Component}/>
                </Switch>
            </App>
        </BrowserRouter>
    </Provider>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
