// Global Shared Services.

export const calculateRecipeLocalItems = (ingredients: Array<any>, ingredient: any) => {
    const newIngredientsArray: { name: string; qty: string; colors: []; }[] = [];

    ingredients.map(item => {
        const { name, qty, colors } = item;
        const qtyCheck = Number(qty);

        if (name === ingredient.name && colors && qtyCheck > 0) {
            const newQty = String(Number(qty) - 1);

            newIngredientsArray.push({
                name,
                qty: newQty,
                colors
            });
        } else if (qtyCheck === 0) {
            alert('There are no more items left to order.');
        } else {
            newIngredientsArray.push(item);
        }

        return true;
    });

    return newIngredientsArray;
};

export const generateDynamicId = (idPrefix: string) => {
    // @ts-ignore
    const getTimeNow = Date.parse(new Date());
    const randNum = Math.random().toString();
    const slicedNum = randNum.slice(3,9);

    return idPrefix + '_' + getTimeNow + slicedNum;
};
