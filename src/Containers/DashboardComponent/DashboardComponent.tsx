import React, {Component} from 'react';
// @ts-ignore
import { connect } from 'react-redux';

import {
    purchaseAnOrder,
    updateAnOrder
} from '../../app-redux/actions/ordersActions';

import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faBars} from '@fortawesome/free-solid-svg-icons';

// import globalSharedServices from '../../globalServices/globalSharedServices';
import {
    calculateRecipeLocalItems,
    // timeLapseFunction,
    generateDynamicId
} from '../../globalServices/globalSharedServices';

import PendingOrdersComponent from '../../Components/PendingOrdersComponent/PendingOrdersComponent';

class DashboardComponent extends Component {
    state = {
        ingredientsSet: false,
        ingredients: [],
        currentFilterColor: '',
        selectionMenuActive: false,
        colorsList: [
            { color_name: 'All', color_slug: 'no_color' },
            { color_name: 'Red', color_slug: 'red' },
            { color_name: 'Blue', color_slug: 'blue' },
            { color_name: 'Green', color_slug: 'green' },
            { color_name: 'Purple', color_slug: 'purple' },
            { color_name: 'Yellow', color_slug: 'yellow' },
            { color_name: 'White', color_slug: 'white' },
            { color_name: 'Cyan', color_slug: 'cyan' }
        ]
    };

    onFulfillOrder = (id: string) => {
        alert(`I will fulfill this order "${id}" on the server after 3 minutes.`);
    };

    onPurchaseAnIngredient = (ingredient: {name: string, qty: string, colors: []}) => {
        const { ingredients } = this.state;
        const newIngredientsArray = calculateRecipeLocalItems(ingredients, ingredient);
        const id = generateDynamicId('order');
        const newIngredientObject = {
            id,
            name: ingredient.name,
            qty: ingredient.qty,
            colors: ingredient.colors
        };

        // Short simulation of how it would be if the client side was connected to a websocket to update this thing.
        setTimeout(() => {
            this.onFulfillOrder(id);
        }, 1000);

        // @ts-ignore
        this.props.purchaseAnOrder(newIngredientObject);
        this.setState({
            ingredients: newIngredientsArray
        });
    };

    onSortIngredients = (colorType: string, ingredients: []) => {
        let setIngredients;
        const sortedArray = (color: string) => {
            const newSortedIngredientsArray: any[] = [];

            // eslint-disable-next-line array-callback-return
            ingredients.map(sortedItem => {
                const { name, qty, colors } = sortedItem;

                if (colors !== undefined) {
                    // @ts-ignore
                    // eslint-disable-next-line array-callback-return
                    colors.map((currentColor: string) => {
                        if (currentColor !== undefined && currentColor === color) {
                            const newIngredient = {
                                name,
                                qty,
                                colors
                            };
                            newSortedIngredientsArray.push(newIngredient);

                            return true;
                        }
                    });
                }

                return true;
            });

            return newSortedIngredientsArray;
        };

        if (colorType === 'red') {
            setIngredients = sortedArray('red');
        } else if (colorType === 'blue') {
            setIngredients = sortedArray('blue');
        } else if (colorType === 'green') {
            setIngredients = sortedArray('green');
        } else if (colorType === 'purple') {
            setIngredients = sortedArray('purple');
        } else if (colorType === 'yellow') {
            setIngredients = sortedArray('yellow');
        } else if (colorType === 'white') {
            setIngredients = sortedArray('white');
        } else if (colorType === 'cyan') {
            setIngredients = sortedArray('cyan');
        } else {
            setIngredients = ingredients
        }

        this.setState({
            ingredientsSet: true,
            ingredients: setIngredients,
            currentFilterColor: colorType
        });
    };

    onToggleSelections() {
        if (!this.state.selectionMenuActive) {
            this.setState({ selectionMenuActive: true });
        } else {
            this.setState({ selectionMenuActive: false });
        }
    }

    onFilterItems = (selection: string) => {
        this.onToggleSelections();
        // @ts-ignore
        this.onSortIngredients(selection, this.props.ingredients_object.ingredients);
    };

    onRenderItemsListItems = () => {
        return this.state.colorsList.map(listItem => {
            const { color_name, color_slug } = listItem;

            return(
                <li
                    key={color_slug}
                    onClick={() => this.onFilterItems(color_slug)}
                    className="app-header-nav-ul-li">
                    {color_name}
                </li>
            );
        });
    };

    onRenderColorsRow = (colors: Array<any>) => {
        return colors.map(color => {
            return(
                <span key={color}>{color}, </span>
            );
        });
    };

    onRenderTableRows = (ingredients: Array<any>) => {
        // eslint-disable-next-line array-callback-return
        return ingredients.map(row => {
            if (row && row.name !== undefined && row.qty !== undefined && row.colors !== undefined) {
                const { name, qty, colors } = row;

                return(
                    <TableRow key={name}>
                        <TableCell component="th" scope="row">{name}</TableCell>
                        <TableCell align="right">{qty}</TableCell>
                        <TableCell align="right">
                            {this.onRenderColorsRow(colors)}
                        </TableCell>
                        <TableCell align="right">
                            <Button
                                onClick={() => this.onPurchaseAnIngredient(row)}
                                variant="outlined">
                                Order
                            </Button>
                        </TableCell>
                    </TableRow>
                );
            }
        });
    };

    onRenderIngredientsTable = () => {
        const { ingredients } = this.state;

        return(
            <TableContainer component={Paper}>
                <Table size="small" aria-label="a dense table">
                    <TableHead>
                        <TableRow>
                            <TableCell>Name</TableCell>
                            <TableCell align="right">Qty</TableCell>
                            <TableCell align="right">Colors</TableCell>
                            <TableCell align="right">Order</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.onRenderTableRows(ingredients)}
                    </TableBody>
                </Table>
            </TableContainer>
        );
    };

    onCheckForIngredients() {
        // @ts-ignore
        if (!this.state.ingredientsSet && this.props.ingredients_object && this.props.ingredients_object.ingredients && this.props.ingredients_object.ingredients.length > 0) {
            // @ts-ignore
            this.onSortIngredients('initial', this.props.ingredients_object.ingredients);
        }
    }

    onCheckStateAndProps() {
        console.log(this.state);
        console.log(this.props);
    }

    componentDidMount() {
        // this.onCheckStateAndProps();
    }

    componentDidUpdate() {
        // this.onCheckStateAndProps();
        this.onCheckForIngredients();
    }

    render() {
        // @ts-ignore
        return(
            <div className="app-wrapper">
                <div className="app-header">
                    <h3 className="app-header-title">Filter:</h3>
                    <button
                        onClick={() => this.onToggleSelections()}
                        className="app-header-toggle-button">
                        <FontAwesomeIcon icon={faBars}/>
                    </button>
                    <nav className={this.state.selectionMenuActive ? "app-header-nav display-element" : "app-header-nav"}>
                        <ul className="app-header-nav-ul">
                            {this.onRenderItemsListItems()}
                        </ul>
                    </nav>
                </div>
                <div className="app-current-color-filter">
                    <span className={this.state.currentFilterColor !== 'initial' && this.state.currentFilterColor !== 'no_color' ? "app-current-color-filter-span" : "hide-display-element"}>
                        filtering on: "{this.state.currentFilterColor}"
                    </span>
                </div>
                {this.onRenderIngredientsTable()}
                <PendingOrdersComponent />
            </div>
        );
    }
}

const mapStateToProps = (state: {
    ingredients: any;
}) => {
    return {
        ingredients_object: state.ingredients
    };
};

export default connect(mapStateToProps, {
    purchaseAnOrder,
    updateAnOrder
})(DashboardComponent);
