import React, {Component} from 'react';
// @ts-ignore
import {Link} from 'react-router-dom';

import Button from '@material-ui/core/Button';

import './TopBarComponent.scss';

class TopBarComponent extends Component {
    state = {
        menuActive: false
    };

    onResetAllData = () => {
        // Remove all data from localStorage and reload the app.
        localStorage.removeItem('app_pending_orders_data');
        localStorage.removeItem('app_orders_data');
        localStorage.removeItem('app_recipes_data');
        localStorage.removeItem('app_ingredients_data');
        window.location.reload();
    };

    componentDidMount() {

    }

    render() {
        return (
            <div className="app-header">
                <Link className="app-header-title" to='/'>Ingredients Tracker</Link>
                <Button
                    onClick={() => this.onResetAllData()}
                    variant="contained">
                    Reset All
                </Button>
            </div>
        );
    }
}

export default TopBarComponent;
