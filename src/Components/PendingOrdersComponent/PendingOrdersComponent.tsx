import React, {Component} from 'react';
// @ts-ignore
import { connect } from 'react-redux';

import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

class PendingOrdersComponent extends Component {
    state = {
        pendingOrdersSet: false,
        pendingOrders: [],
    };

    onCancelOrder = (orderId: string) => {
        alert(orderId);
    };

    onRenderRows = (orders: Array<any>) => {
        let keyCount = 0;

        // eslint-disable-next-line array-callback-return
        return orders.map(order => {
            keyCount += 1;

            if (order && order.name && order.id !== undefined) {
                const { name, id } = order;

                return(
                    <TableRow key={keyCount}>
                        <TableCell component="th" scope="row">{name}</TableCell>
                        <TableCell component="th" scope="row">{id}</TableCell>
                        <TableCell align="right">
                            <Button
                                onClick={() => this.onCancelOrder(id)}
                                variant="outlined">
                                Cancel Order
                            </Button>
                        </TableCell>
                    </TableRow>
                );
            }
        });
    };

    onRenderTable = () => {
        // @ts-ignore
        if (this.props.orders_object && this.props.orders_object.orders && this.props.orders_object.orders.length > 0) {
            // @ts-ignore
            const { orders } = this.props.orders_object;

            return(
                <TableContainer component={Paper}>
                    <Table size="small" aria-label="a dense table">
                        <TableHead>
                            <TableRow>
                                <TableCell>Name</TableCell>
                                <TableCell>ID</TableCell>
                                <TableCell align="right">Action</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {this.onRenderRows(orders)}
                        </TableBody>
                    </Table>
                </TableContainer>
            );
        } else {
            return <div>No Pending Orders</div>
        }
    };

    render() {
        // @ts-ignore
        return(
            <div className="app-wrapper">
                <div className="app-header sub-header">
                    <h3 className="app-header-title">Pending Orders</h3>
                </div>
                <div className="app-orders">
                    {this.onRenderTable()}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state: {
    orders: any;
}) => {
    return {
        orders_object: state.orders
    };
};

export default connect(mapStateToProps, {

})(PendingOrdersComponent);
