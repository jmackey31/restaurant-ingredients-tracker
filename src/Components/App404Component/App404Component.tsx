import React from 'react';

import './App404Component.scss';

const App404Component = () => {
    return(
        <div className="app-error-page">
            <div className="app-error-page-inner-wrap">
                <h1>404</h1>
                <p>NOT FOUND</p>
            </div>
        </div>
    );
};

export default App404Component;
