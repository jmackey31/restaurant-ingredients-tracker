import React, {Component} from 'react';
// @ts-ignore
import { connect } from 'react-redux';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
    faStopCircle,
    faClock,
    faCog,
    faFlag
} from '@fortawesome/free-solid-svg-icons';

import {
    fetchIngredients,
    fetchStoredIngredients
} from '../../app-redux/actions/ingredientsActions';

import {
    fetchOrders,
    fetchStoredOrders
} from '../../app-redux/actions/ordersActions';
import {
    fetchRecipes,
    fetchStoredRecipes
} from '../../app-redux/actions/recipesActions';

import './StatusBarComponent.scss';

class StatusBarComponent extends Component {
    state = {
        ingredientsSet: false,
        ingredientsArray: [],
        ordersSet: false,
        ordersArray: [],
        recipesSet: false,
        recipesArray: [],
        cancelledCount: 0,
        pendingCount: 0,
        inProgressCount: 0,
        fulfilledCount: 0
    };

    onCheckStateAndProps() {
        console.log(this.state);
        console.log(this.props);
    }

    onFetchData() {
        if (!localStorage.getItem('app_ingredients_data')) {
            // @ts-ignore
            this.props.fetchIngredients();
        } else {
            // @ts-ignore
            this.props.fetchStoredIngredients();
        }
        if (!localStorage.getItem('app_orders_data')) {
            // @ts-ignore
            this.props.fetchOrders();
        } else {
            // @ts-ignore
            this.props.fetchStoredOrders();
        }
        if (!localStorage.getItem('app_recipes_data')) {
            // @ts-ignore
            this.props.fetchRecipes();
        } else {
            // @ts-ignore
            this.props.fetchStoredRecipes();
        }
    }

    onSortOrders = (orders: []) => {
        let cancelledCount = 0;
        let pendingCount = 0;
        let inProgressCount = 0;
        let fulfilledCount = 0;

        orders.map(order => {
            const { cancelled, pending, inProgress, fulfilled } = order;

            if (cancelled) {
                cancelledCount += 1;
            } else if (pending) {
                pendingCount += 1;
            } else if (inProgress) {
                inProgressCount += 1;
            } else if (fulfilled) {
                fulfilledCount += 1;
            }

            return true;
        });

        this.setState({
            cancelledCount,
            pendingCount,
            inProgressCount,
            fulfilledCount
        });
    };

    onPopulateStateFromUpdatedProps = (collection: string, args: []) => {
        switch (collection) {
            case 'ingredients':
                this.setState({
                    ingredientsSet: true,
                    ingredientsArray: args
                });
                break;
            case 'orders':
                this.onSortOrders(args);
                this.setState({
                    ordersSet: true,
                    ordersArray: args
                });
                break;
            case 'recipes':
                this.setState({
                    recipesSet: true,
                    recipesArray: args
                });
                break;
            default:
                // Do nothing.
        }
    };

    onCheckForDataUpdates() {
        // @ts-ignore
        if (!this.state.ingredientsSet && this.props.ingredients_object && this.props.ingredients_object.ingredients && this.props.ingredients_object.ingredients.length > 0) {
            // @ts-ignore
            this.onPopulateStateFromUpdatedProps('ingredients', this.props.ingredients_object.ingredients);
            // @ts-ignore
        } else if (!this.state.ordersSet && this.props.orders_object && this.props.orders_object.orders && this.props.orders_object.orders.length > 0) {
            // @ts-ignore
            this.onPopulateStateFromUpdatedProps('orders', this.props.orders_object.orders);
            // @ts-ignore
        } else if (this.state.ordersSet && this.props.orders_object && this.props.orders_object.ordersUpdate && this.props.orders_object.orders && this.props.orders_object.orders.length > 0) {
            // @ts-ignore
            this.props.fetchStoredOrders();
            // @ts-ignore
            this.onPopulateStateFromUpdatedProps('orders', this.props.orders_object.orders);
            // @ts-ignore
        } else if (!this.state.recipesSet && this.props.recipes_object && this.props.recipes_object.recipes && this.props.recipes_object.recipes.length > 0) {
            // @ts-ignore
            this.onPopulateStateFromUpdatedProps('recipes', this.props.recipes_object.recipes);
        }
    }

    componentDidMount() {
        this.onFetchData();
    }

    componentDidUpdate() {
        // this.onCheckStateAndProps();
        this.onCheckForDataUpdates();
        // this.onUpdatePendingOrdersData();
    }

    render() {
        const { cancelledCount, pendingCount, inProgressCount, fulfilledCount } = this.state;

        return (
            <div className="app-status-bar">
                <div className="app-status-bar-block red-bg">
                    <FontAwesomeIcon icon={faStopCircle}/>
                    <span className="app-status-bar-block-span">({cancelledCount}) Cancelled Orders</span>
                </div>
                <div className="app-status-bar-block yellow-bg">
                    <FontAwesomeIcon icon={faClock}/>
                    <span className="app-status-bar-block-span">({pendingCount}) Pending Orders</span>
                    {/*<span className="app-status-bar-block-span">({this.onUpdatePendingOrdersData()}) Pending Orders</span>*/}
                </div>
                <div className="app-status-bar-block blue-bg">
                    <FontAwesomeIcon icon={faCog}/>
                    <span className="app-status-bar-block-span">({inProgressCount}) In-Progress Orders</span>
                </div>
                <div className="app-status-bar-block green-bg">
                    <FontAwesomeIcon icon={faFlag}/>
                    <span className="app-status-bar-block-span">({fulfilledCount}) Fulfilled Orders</span>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state: {
    ingredients: any;
    orders: any;
    recipes: any;
}) => {
    return {
        ingredients_object: state.ingredients,
        orders_object: state.orders,
        recipes_object: state.recipes
    };
};

export default connect(mapStateToProps, {
    fetchIngredients,
    fetchStoredIngredients,
    fetchOrders,
    fetchStoredOrders,
    fetchRecipes,
    fetchStoredRecipes
})(StatusBarComponent);
