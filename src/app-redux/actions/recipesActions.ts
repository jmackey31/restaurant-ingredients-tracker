import axios from 'axios';

import { apiEnvs } from '../../configs/configs';

// @ts-ignore
export const fetchRecipes = () => async dispatch => {
    const url = `${apiEnvs.api_uri}/recipes`;
    const response = await axios({
        method: 'GET',
        url
    });

    dispatch({
        type: 'FETCH_RECIPES',
        payload: response.data
    });
};

// @ts-ignore
export const fetchStoredRecipes = () => async dispatch => {
    dispatch({
        type: 'LOAD_LOCAL_RECIPES'
    });
};
