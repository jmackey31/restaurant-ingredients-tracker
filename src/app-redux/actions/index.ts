import { combineReducers } from 'redux';
import ingredientsReducer from '../reducers/ingredientsReducer';
import ordersReducer from '../reducers/ordersReducer';
import recipesReducer from '../reducers/recipesReducer';

export default combineReducers({
    ingredients_object: ingredientsReducer,
    orders_object: ordersReducer,
    recipes_object: recipesReducer
});
