import axios from 'axios';

import { apiEnvs } from '../../configs/configs';

// @ts-ignore
export const purchaseAnOrder = (ingredient: any) => async dispatch => {
    dispatch({
        type: 'PURCHASE_AN_ORDER',
        payload: ingredient
    });
};

// @ts-ignore
export const updateAnOrder = (order: { id: any; }) => async dispatch => {
    dispatch({
        type: 'UPDATE_AN_ORDER',
        payload: order
    });
};

// @ts-ignore
export const fetchOrders = () => async dispatch => {
    const url = `${apiEnvs.api_uri}/orders`;
    const response = await axios({
        method: 'GET',
        url
    });

    dispatch({
        type: 'FETCH_ORDERS',
        payload: response.data
    });
};

// @ts-ignore
export const fetchStoredOrders = () => async dispatch => {
    dispatch({
        type: 'LOAD_LOCAL_ORDERS'
    });
};
