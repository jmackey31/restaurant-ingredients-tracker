import axios from 'axios';

import { apiEnvs } from '../../configs/configs';

// @ts-ignore
export const fetchIngredients = () => async dispatch => {
    const url = `${apiEnvs.api_uri}/items`;
    const response = await axios({
        method: 'GET',
        url
    });

    dispatch({
        type: 'FETCH_INGREDIENTS',
        payload: response.data
    });
};

// @ts-ignore
export const fetchStoredIngredients = () => async dispatch => {
    dispatch({
        type: 'LOAD_LOCAL_INGREDIENTS'
    });
};
