// @ts-ignore
export default (state = [], action) => {
    switch (action.type) {
        case 'PURCHASE_AN_INGREDIENT':
            if (action.payload.success) {
                return {
                    ...state,
                    ingredient: action.payload.iten
                };
            } else {
                return {
                    ...state,
                    ingredient: {}
                };
            }
        case 'UPDATE_AN_INGREDIENT':
            if (action.payload.success) {
                return {
                    ...state,
                    ingredient: action.payload.iten
                };
            } else {
                return {
                    ...state,
                    ingredient: {}
                };
            }
        case 'FETCH_INGREDIENTS':
            localStorage.setItem('app_ingredients_data', JSON.stringify(action.payload.itens));

            return {
                ...state,
                ingredients: action.payload.itens
            };
        case 'LOAD_LOCAL_INGREDIENTS':
            return {
                ...state,
                // @ts-ignore
                ingredients: JSON.parse(localStorage.getItem('app_ingredients_data'))
            };
        default:
            return state;
    }
};
