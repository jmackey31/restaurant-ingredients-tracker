// @ts-ignore
export default (state = [], action) => {
    switch (action.type) {
        case 'PURCHASE_AN_RECIPE':
            if (action.payload.success) {
                return {
                    ...state,
                    recipe: action.payload.recipes
                };
            } else {
                return {
                    ...state,
                    recipe: {}
                };
            }
        case 'UPDATE_AN_RECIPE':
            if (action.payload.success) {
                return {
                    ...state,
                    recipe: action.payload.recipes
                };
            } else {
                return {
                    ...state,
                    recipe: {}
                };
            }
        case 'FETCH_RECIPES':
            localStorage.setItem('app_recipes_data', JSON.stringify(action.payload.recipes));

            return {
                ...state,
                recipes: action.payload.recipes
            };
        case 'LOAD_LOCAL_RECIPES':
            return {
                ...state,
                // @ts-ignore
                recipes: JSON.parse(localStorage.getItem('app_recipes_data'))
            };
        default:
            return state;
    }
};
