import {
    calculateRecipeLocalItems
} from '../../globalServices/globalSharedServices';

// @ts-ignore
export default (state = [], action) => {
    switch (action.type) {
        case 'PURCHASE_AN_ORDER':
            // Simulating how this would be handled if data was
            // being sent to and from the server.
            let newPendingOrdersArray = [];
            const currentIngredients = JSON.parse(<string>localStorage.getItem('app_ingredients_data'));
            const newIngredientsArray = calculateRecipeLocalItems(currentIngredients, action.payload);
            const currentOrders = JSON.parse(<string>localStorage.getItem('app_orders_data'));
            const pendingOrders = JSON.parse(<string>localStorage.getItem('app_pending_orders_data'));
            const newOrderObject = {
                id: action.payload.id,
                pending: true,
                name: action.payload.name
            };
            currentOrders.push(newOrderObject);
            if (pendingOrders) {
                pendingOrders.push(newOrderObject);
                newPendingOrdersArray = pendingOrders;
            } else {
                newPendingOrdersArray.push(newIngredientsArray);
            }

            // Setting the orders data.
            localStorage.setItem('app_orders_data', JSON.stringify(currentOrders));

            // Setting the ingredients data.
            localStorage.setItem('app_ingredients_data', JSON.stringify(newIngredientsArray));

            // Setting the pending data.
            localStorage.setItem('app_pending_orders_data', JSON.stringify(newPendingOrdersArray));

            return {
                ...state,
                ordersUpdate: true,
                orders: currentOrders
            };
        case 'UPDATE_AN_ORDER':
            return true;
        case 'FETCH_ORDERS':
            localStorage.setItem('app_orders_data', JSON.stringify(action.payload.orders));

            return {
                ...state,
                orders: action.payload.orders
            };
        case 'LOAD_LOCAL_ORDERS':
            return {
                ...state,
                ordersUpdate: false,
                // @ts-ignore
                orders: JSON.parse(localStorage.getItem('app_orders_data'))
            };
        default:
            return state;
    }
};
