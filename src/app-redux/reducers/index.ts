import { combineReducers } from 'redux';
import ingredientsReducer from './ingredientsReducer';
import ordersReducer from './ordersReducer';
import recipesReducer from './recipesReducer';

export default combineReducers({
    ingredients: ingredientsReducer,
    orders: ordersReducer,
    recipes: recipesReducer
});
