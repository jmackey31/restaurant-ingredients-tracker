import React, {Component} from 'react';

import './App.scss';

class App extends Component<{ children: any }> {
    render() {
        let {children} = this.props;

        return (
            <div className="app-wrapper">
                {children}
            </div>
        );
    }
}

export default App;
